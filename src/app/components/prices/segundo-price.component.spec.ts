import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SegundoPriceComponent } from './segundo-price.component';

describe('SegundoPriceComponent', () => {
  let component: SegundoPriceComponent;
  let fixture: ComponentFixture<SegundoPriceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SegundoPriceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SegundoPriceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
